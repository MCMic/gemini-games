<?php

require __DIR__ . '/vendor/autoload.php';

$domain = 'lanterne.chilliet.eu';

use MCMic\Gemini;

$server = new Gemini\Server(1965, new Gemini\RequestCsvLogger(__DIR__ . '/requests.csv'));

$handlers = [
    new Gemini\TicTacToe\RequestHandler('tictactoe.' . $domain, __DIR__ . '/tictactoe.pem'),
    new Gemini\Chess\PuzzleHandler('chess.' . $domain, __DIR__ . '/chess.pem'),
    new Gemini\Files\RequestHandler(
        'code.' . $domain,
        __DIR__ . '/code.pem',
        __DIR__,
        [
            'LICENSE','gemini-games.php','src','src/*.php','src/*/*.php',
            'vendor/mcmic/gemini-server/*',
            'vendor/mcmic/gemini-server/src/*.php',
            'vendor/mcmic/gemini-server/src/*/*.php',
        ]
    ),
    new Gemini\Files\RequestHandler('games.' . $domain, __DIR__ . '/games.pem', __DIR__ . '/games', ['*.gmi'], null, 'en'),
    new Gemini\Files\RequestHandler('gemlog.' . $domain, __DIR__ . '/gemlog.pem', __DIR__ . '/gemlog', ['*.gmi'], 'Gemlog de MCMic', 'fr'),
    new Gemini\Files\RequestHandler('français.' . $domain, __DIR__ . '/français.pem', __DIR__ . '/français', ['*.gmi'], 'Recensement du Geminispace francophone', 'fr'),
];

$handlers[0]->setFavicon('⭕');
$handlers[1]->setFavicon('🮖');
$handlers[2]->setFavicon('🛠');
$handlers[3]->setFavicon('🎲');
$handlers[4]->setFavicon('🐼');
$handlers[5]->setFavicon('🇫🇷');

$handlers[0]->setRobotsRules(
    "# Avoid having robots playing tic tac toe\r\n" .
    "User-agent: archiver\r\nDisallow: /\r\n" .
    "User-agent: indexer\r\nDisallow: /\r\n" .
    "User-agent: researcher\r\nDisallow: /\r\n" .
    "# Bots supporting Allow may crawl main page\r\n" .
    "User-agent: *\r\nAllow: /---------\r\n"
);
foreach (range(1, 5) as $i) {
    $handlers[$i]->setRobotsRules(
        "# Happy crawling!\r\n"
    );
}

foreach ($handlers as $handler) {
    $server->addHandler($handler);
}

$server->serve();
