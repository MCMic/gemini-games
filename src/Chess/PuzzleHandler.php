<?php

declare(strict_types=1);

namespace MCMic\Gemini\Chess;

use MCMic\Gemini;

class PuzzleHandler extends Gemini\RequestHandler
{
    public function handle(Gemini\Request $request): Gemini\Response
    {
        $stateString = substr($request->path ?? '/', 1);
        if ($stateString !== '') {
            throw new Gemini\Exception('Invalid request "' . $stateString . '"', 59);
        }

        $content = file_get_contents('https://api.chess.com/pub/puzzle/random');
        if ($content === false) {
            throw new Gemini\Exception('Chess.com API did not return valid data', 42);
        }

        $puzzle = json_decode($content, true, 10, JSON_THROW_ON_ERROR);
        if (!isset($puzzle['fen'])) {
            throw new Gemini\Exception('Chess.com API did not return valid data', 42);
        }

        $output = '# Chess.com random puzzle' . "\r\n";
        $output .= "\r\n";

        if (isset($puzzle['title'])) {
            $output .= '## ' . $puzzle['title'] . "\r\n";
        }

        $board = new Board($puzzle['fen']);
        $output .= '```' . "\r\n";
        $output .= $board->render();
        $output .= '```' . "\r\n";

        $output .= $board->renderInformation(false);
        $output .= $this->getFooter($puzzle);

        return new Gemini\Response\Success($output, 'text/gemini', 'en');
    }

    /**
     * @param array<string,string> $puzzle
     */
    protected function getFooter(array $puzzle): string
    {
        $footer = "\r\n";
        $footer .= '## Information' . "\r\n";
        $footer .= "\r\n";
        $footer .= '* Made with PHP by MCMic' . "\r\n";
        if (isset($puzzle['url'])) {
            $footer .= '=> ' . $puzzle['url'] . ' Open the puzzle on chess.com' . "\r\n";
        }
        $footer .= '=> gemini://code.lanterne.chilliet.eu Source code' . "\r\n";
        $footer .= '=> https://framagit.org/MCMic/gemini-games.git Git repository' . "\r\n";
        $footer .= "\r\n";
        return $footer;
    }
}
