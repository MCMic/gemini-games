<?php

declare(strict_types=1);

namespace MCMic\Gemini;

class UnicodeBoard
{
    /**
     * @var array<array<UnicodeBoardSquare>>
     */
    protected array $state = [];

    protected const OUTOFBOUNDS = 0;
    protected const CASECONTENT = 1;
    protected const HIGHLIGHTED = 2;

    public function __construct(int $lines, int $cols)
    {
        for ($l = 0; $l < $lines; $l++) {
            $this->state[] = [];
            for ($c = 0; $c < $cols; $c++) {
                $this->state[$l][] = new UnicodeBoardSquare();
            }
        }
    }

    public function getLeftContent(int $l, int $pos): string
    {
        return '';
    }

    public function getRightContent(int $l, int $pos): string
    {
        return "\r\n";
    }

    public function render(): string
    {
        $separators = (count($this->state[0] ?? []) - 1);
        if ($separators < 0) {
            return '';
        }
        $output = $this->getLeftContent(0, 0) . $this->getTopBorderLine(0) . $this->getRightContent(0, 0);
        $l = 0;
        foreach ($this->state as $line) {
            $c = 0;
            $output .= $this->getLeftContent($l, 1);
            $output .= static::getBorderVertical(static::OUTOFBOUNDS, $this->getBorderType($l, $c));
            foreach ($line as $case) {
                $output .= ' ';
                $output .= $case->content;
                if ($case->highlight) {
                }
                $output .= ' ';
                $output .= static::getBorderVertical($this->getBorderType($l, $c), $this->getBorderType($l, $c + 1));
                $c++;
            }
            $output .= $this->getRightContent($l, 1);
            $l++;
            $output .= $this->getLeftContent($l, 2);
            $output .= $this->getTopBorderLine($l);
            $output .= $this->getRightContent($l, 2);
        }

        return $output;
    }

    protected function getBorderType(int $l, int $c): int
    {
        if (isset($this->state[$l][$c])) {
            return ($this->state[$l][$c]->highlight ? static::HIGHLIGHTED : static::CASECONTENT);
        } else {
            return static::OUTOFBOUNDS;
        }
    }

    protected function getTopBorderLine(int $l): string
    {
        $output = static::getBorderCorner(
            static::OUTOFBOUNDS,
            $this->getBorderType($l - 1, 0),
            static::OUTOFBOUNDS,
            $this->getBorderType($l, 0)
        );
        for ($c = 0; $c < count($this->state[$l] ?? $this->state[$l - 1]); $c++) {
            $output .= str_repeat(
                static::getBorderHorizontal(
                    $this->getBorderType($l - 1, $c),
                    $this->getBorderType($l, $c),
                ),
                3
            );
            $output .= static::getBorderCorner(
                $this->getBorderType($l - 1, $c),
                $this->getBorderType($l - 1, $c + 1),
                $this->getBorderType($l, $c),
                $this->getBorderType($l, $c + 1),
            );
        }
        return $output;
    }

    protected static function getBorderCorner(int $lt, int $rt, int $lb, int $rb): string
    {
        switch ($lt) {
            case UnicodeBoard::OUTOFBOUNDS:
                switch ($rt) {
                    case UnicodeBoard::OUTOFBOUNDS:
                        switch ($lb) {
                            case UnicodeBoard::OUTOFBOUNDS:
                                switch ($rb) {
                                    case UnicodeBoard::CASECONTENT:
                                        return '┌';
                                    case UnicodeBoard::HIGHLIGHTED:
                                        return '┏';
                                }
                            case UnicodeBoard::CASECONTENT:
                                switch ($rb) {
                                    case UnicodeBoard::CASECONTENT:
                                        return '┬';
                                    case UnicodeBoard::HIGHLIGHTED:
                                        return '┲';
                                    case UnicodeBoard::OUTOFBOUNDS:
                                        return '┐';
                                }
                            case UnicodeBoard::HIGHLIGHTED:
                                switch ($rb) {
                                    case UnicodeBoard::CASECONTENT:
                                        return '┱';
                                    case UnicodeBoard::HIGHLIGHTED:
                                        return '┳';
                                    case UnicodeBoard::OUTOFBOUNDS:
                                        return '┓';
                                }
                        }
                    case UnicodeBoard::CASECONTENT:
                        switch ($lb) {
                            case UnicodeBoard::OUTOFBOUNDS:
                                switch ($rb) {
                                    case UnicodeBoard::CASECONTENT:
                                        return '├';
                                    case UnicodeBoard::HIGHLIGHTED:
                                        return '┢';
                                    case UnicodeBoard::OUTOFBOUNDS:
                                        return '└';
                                }
                        }
                    case UnicodeBoard::HIGHLIGHTED:
                        switch ($lb) {
                            case UnicodeBoard::OUTOFBOUNDS:
                                switch ($rb) {
                                    case UnicodeBoard::CASECONTENT:
                                        return '┡';
                                    case UnicodeBoard::HIGHLIGHTED:
                                        return '┣';
                                    case UnicodeBoard::OUTOFBOUNDS:
                                        return '┗';
                                }
                        }
                }
            case UnicodeBoard::CASECONTENT:
                switch ($rt) {
                    case UnicodeBoard::OUTOFBOUNDS:
                        switch ($lb) {
                            case UnicodeBoard::OUTOFBOUNDS:
                                return '┘';
                            case UnicodeBoard::CASECONTENT:
                                return '┤';
                            case UnicodeBoard::HIGHLIGHTED:
                                return '┪';
                        }
                    case UnicodeBoard::CASECONTENT:
                        switch ($lb) {
                            case UnicodeBoard::OUTOFBOUNDS:
                                return '┴';
                            case UnicodeBoard::CASECONTENT:
                                switch ($rb) {
                                    case UnicodeBoard::CASECONTENT:
                                        return '┼';
                                    case UnicodeBoard::HIGHLIGHTED:
                                        return '╆';
                                }
                            case UnicodeBoard::HIGHLIGHTED:
                                switch ($rb) {
                                    case UnicodeBoard::CASECONTENT:
                                        return '╅';
                                    case UnicodeBoard::HIGHLIGHTED:
                                        return '╈';
                                }
                        }
                    case UnicodeBoard::HIGHLIGHTED:
                        switch ($lb) {
                            case UnicodeBoard::OUTOFBOUNDS:
                                return '┺';
                            case UnicodeBoard::CASECONTENT:
                                switch ($rb) {
                                    case UnicodeBoard::CASECONTENT:
                                        return '╄';
                                    case UnicodeBoard::HIGHLIGHTED:
                                        return '╊';
                                }
                            case UnicodeBoard::HIGHLIGHTED:
                                return '╋';
                        }
                }
            case UnicodeBoard::HIGHLIGHTED:
                switch ($rt) {
                    case UnicodeBoard::OUTOFBOUNDS:
                        switch ($lb) {
                            case UnicodeBoard::OUTOFBOUNDS:
                                return '┛';
                            case UnicodeBoard::CASECONTENT:
                                return '┩';
                            case UnicodeBoard::HIGHLIGHTED:
                                return '┫';
                        }
                    case UnicodeBoard::CASECONTENT:
                        switch ($lb) {
                            case UnicodeBoard::OUTOFBOUNDS:
                                return '┹';
                            case UnicodeBoard::CASECONTENT:
                                switch ($rb) {
                                    case UnicodeBoard::CASECONTENT:
                                        return '╃';
                                    case UnicodeBoard::HIGHLIGHTED:
                                        return '╋';
                                }
                            case UnicodeBoard::HIGHLIGHTED:
                                switch ($rb) {
                                    case UnicodeBoard::CASECONTENT:
                                        return '╉';
                                    case UnicodeBoard::HIGHLIGHTED:
                                        return '╋';
                                }
                        }
                    case UnicodeBoard::HIGHLIGHTED:
                        switch ($lb) {
                            case UnicodeBoard::OUTOFBOUNDS:
                                return '┺';
                            case UnicodeBoard::CASECONTENT:
                                switch ($rb) {
                                    case UnicodeBoard::CASECONTENT:
                                        return '╄';
                                    case UnicodeBoard::HIGHLIGHTED:
                                        return '╊';
                                }
                            case UnicodeBoard::HIGHLIGHTED:
                                return '╋';
                        }
                }
        }
        return '.';
    }

    protected static function getBorderHorizontal(int $t, int $b): string
    {
        if (($t === UnicodeBoard::HIGHLIGHTED) || ($b === UnicodeBoard::HIGHLIGHTED)) {
            return '━';
        } else {
            return '─';
        }
    }

    protected static function getBorderVertical(int $l, int $r): string
    {
        if (($l === UnicodeBoard::HIGHLIGHTED) || ($r === UnicodeBoard::HIGHLIGHTED)) {
            return '┃';
        } else {
            return '│';
        }
    }

    public function setSquare(int $l, int $c, string $content, bool $highlight): void
    {
        $this->state[$l][$c]->content = $content;
        $this->state[$l][$c]->highlight = $highlight;
    }
}
