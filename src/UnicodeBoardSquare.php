<?php

declare(strict_types=1);

namespace MCMic\Gemini;

class UnicodeBoardSquare
{
    public bool $highlight = false;
    public string $content = ' ';
}
