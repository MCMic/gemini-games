<?php

require __DIR__ . '/vendor/autoload.php';

use MCMic\Gemini;

$server = new Gemini\Server();

$server->addHandler(
    new Gemini\TicTacToe\RequestHandler('tictactoe.example.com', __DIR__ . '/tictactoe.pem')
);

$server->serve();
